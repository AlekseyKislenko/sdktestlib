# sdktestlib

[![CI Status](http://img.shields.io/travis/Aleksey Kislenko/sdktestlib.svg?style=flat)](https://travis-ci.org/Aleksey Kislenko/sdktestlib)
[![Version](https://img.shields.io/cocoapods/v/sdktestlib.svg?style=flat)](http://cocoapods.org/pods/sdktestlib)
[![License](https://img.shields.io/cocoapods/l/sdktestlib.svg?style=flat)](http://cocoapods.org/pods/sdktestlib)
[![Platform](https://img.shields.io/cocoapods/p/sdktestlib.svg?style=flat)](http://cocoapods.org/pods/sdktestlib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

sdktestlib is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'sdktestlib'
```

## Author

Aleksey Kislenko, a.kislenko@newline.tech

## License

sdktestlib is available under the MIT license. See the LICENSE file for more info.
