#
# Be sure to run `pod lib lint sdktestlib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'sdktestlib'
  s.version          = '0.1.1'
  s.summary          = 'This description is used to generate tags'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This description is used to generate tags and improve search results.'

  s.homepage         = 'https://bitbucket.org/AlekseyKislenko/sdktestlib'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aleksey Kislenko' => 'a.kislenko@newline.tech' }
  s.source           = { :git => 'https://bitbucket.org/AlekseyKislenko/sdktestlib', :tag => s.version.to_s }
  s.vendored_frameworks = 'sdktestlib/SDK_Teragence.framework'
other_ldflags = '-ObjC' + '-framework' + '-lz'  + '-lstdc++'
  s.xcconfig     = {
'ARCHS'  => 'arm64 armv7 armv7s',
'VALID_ARCHS'  => 'arm64 armv7 armv7s',
"OTHER_LDFLAGS[arch=arm64]"  => other_ldflags,
"OTHER_LDFLAGS[arch=armv7]"  => other_ldflags,
"OTHER_LDFLAGS[arch=armv7s]" => other_ldflags,
"OTHER_LDFLAGS[arch=i386]" => other_ldflags,
"OTHER_LDFLAGS[arch=x86_64]" => other_ldflags,
'ENABLE_BITCODE' => 'NO'
#"OTHER_LDFLAGS"  => other_ldflags
  }

  s.requires_arc = true
  s.social_media_url = 'http://www.teragence.com'
  s.ios.deployment_target = '8.0'
  s.source_files = 'sdktestlib/SDK_Teragence.framework/Versions/A/Headers/*.h'
  s.resources = "sdktestlib/Assets/*"
# s.source_files = 'sdktestlib/SDK_Teragence.framework'
  s.public_header_files = 'sdktestlib/SDK_Teragence.framework/Versions/A/Headers/*.h'
  s.frameworks = 'Foundation', 'CoreLocation'
  s.library = 'xml2'
s.resource_bundles = {
'Resources' => ['sdktestlib/Assets/*.plist']
}

end
