//
//  SDK_Teragence.h
//  SDK_Teragence
//
//  Created by Andrii Mazepa on 9/11/17.
//  Copyright © 2017 NLT. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SDK_Teragence.
FOUNDATION_EXPORT double SDK_TeragenceVersionNumber;

//! Project version string for SDK_Teragence.
FOUNDATION_EXPORT const unsigned char SDK_TeragenceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SDK_Teragence/PublicHeader.h>

#import <SDK_Teragence/TRGController.h>
