//
//  TRGController.h
//  SDK_Teragence
//
//  Created by Andrii Mazepa on 9/11/17.
//  Copyright © 2017 NLT. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, TestOption) {
    TestOptionDownload = 1 << 0,
    TestOptionBurst = 1 << 1,
    TestOptionPing = 1 << 2,
};

@interface TRGController : NSObject

@property (nonatomic) BOOL isRunned;
@property (nonatomic) BOOL enabledBackgroundMode;

- (instancetype)initWithPartnerID:(NSString *)partnerID;
- (void)makeManualMesurements;
@end

